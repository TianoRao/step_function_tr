# AWS Lambda Rust Project with Step Functions

This project demonstrates the use of AWS Lambda functions written in Rust, orchestrated by AWS Step Functions to create a data processing pipeline. It involves calculating financial option prices using the Black-Scholes model, storing results in DynamoDB, and aggregating data.

## Project Structure

- `lambda_black_scholes`: Lambda function for calculating call prices using the Black-Scholes formula.
- `lambda_store_results`: Lambda function for storing calculation results in DynamoDB.
- `lambda_aggregate_data`: Lambda function for aggregating and counting results from DynamoDB.
- `step_functions_workflow`: AWS Step Functions State Machine that coordinates the workflow of the above Lambda functions.

## Requirements

- AWS CLI
- Rust and Cargo
- AWS CDK or Serverless Framework for deployment (optional)
- AWS Account and configured AWS credentials

## Setup and Deployment

### Local Setup

1. clone my repo first
```
git clone https://gitlab.com/TianoRao/step_function_tr
cd step_function_tr
```

2. build all the steps using
```
cargo lambda build <your_steps>
```

3. deploy all the steps using
```
cargo lambda deploy <your_step>
```

4. test if all steps can be properly ran on AWS lambda function test

5. go to test functions, and add all the steps and start step execution

### Deployment

Use AWS to define and deploy your Lambda functions and Step Functions workflow:
![alt text](<imgs/Screenshot 2024-04-17 at 10.37.28 PM.png>)


## Usage

Once deployed, you can invoke the Step Functions State Machine, which will execute the Lambdas in order based on the defined workflow.

## Demo Video

A short demo video illustrating the execution of the state machine and Lambda functions can be found at (here)[https://youtu.be/E2jR6SDOas8].

