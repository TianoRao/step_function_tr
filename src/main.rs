use lambda_runtime::{run, service_fn, LambdaEvent, Error};
use serde::{Deserialize, Serialize};
use statrs::distribution::{ContinuousCDF, Normal};
use std::f64::consts::E;
use tracing_subscriber::{EnvFilter, fmt::SubscriberBuilder};
use tracing_subscriber::filter::LevelFilter;


#[derive(Deserialize)]
struct Request {
    s0: f64,
    x: f64,
    r: f64,
    sigma: f64,
    t: f64,
}

#[derive(Serialize)]
struct Response {
    message: String,
    call_price: f64,
}

fn black_scholes_call_price(s0: f64, x: f64, r: f64, sigma: f64, t: f64) -> f64 {
    let d1 = (s0.ln() / x + (r + sigma.powi(2) / 2.0) * t) / (sigma * t.sqrt());
    let d2 = d1 - sigma * t.sqrt();
    let norm_dist = statrs::distribution::Normal::new(0.0, 1.0).unwrap();
    let nd1 = norm_dist.cdf(d1);
    let nd2 = norm_dist.cdf(d2);
    s0 * nd1 - x * std::f64::consts::E.powf(-r * t) * nd2
}

async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    let params = event.payload;
    let call_price = black_scholes_call_price(params.s0, params.x, params.r, params.sigma, params.t);

    Ok(Response {
        message: event.context.request_id,
        call_price,
    })
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    // lambda_runtime::tracing::init_default_subscriber();
    run(service_fn(function_handler)).await
}
