use aws_sdk_dynamodb::{Client, model::AttributeValue};
use serde::{Deserialize, Serialize};
use lambda_runtime::{run, service_fn, Error, LambdaEvent};

#[derive(Debug, Serialize, Deserialize)]
pub struct OptionResult {
    message: String,
    call_price: f64,
}

#[derive(Deserialize, Serialize)] // Make sure Deserialize and Serialize are derived
struct Request {
    message: String,
    call_price: f64,
}

#[derive(Debug, Serialize)] // Implement Serialize to ensure it can be returned
struct FailureResponse {
    pub body: String,
}

impl std::fmt::Display for FailureResponse {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.body)
    }
}

#[derive(Serialize)] // Make sure Serialize is derived
struct Response {
    req_id: String,
    msg: String,
}

async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    let req_id = event.payload.message;
    let call_price = event.payload.call_price;
    let config = aws_config::load_from_env().await;
    let client = Client::new(&config);

    let calls = OptionResult {
        message: req_id.clone(),
        call_price,
    };

    let call_id = AttributeValue::S(calls.message.clone());
    let call_price_value = AttributeValue::N(calls.call_price.to_string());

    let _resp = client
        .put_item()
        .table_name("OptionResults")
        .item("result_id", call_id)
        .item("call_price", call_price_value)
        .send()
        .await
        .map_err(|_err| Error::from(_err.to_string()))?;

    // Prepare the response
    let resp = Response {
        req_id,
        msg: "Inserted into db".to_string(),
    };

    // Return `Response` (it will be serialized to JSON automatically by the runtime)
    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    run(service_fn(function_handler)).await
}
